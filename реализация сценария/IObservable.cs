﻿using System;
using System.Collections.Generic;
using System.Text;

namespace practice_2
{
    //паттерн наблюдатель
    interface IObservable
    {
        void RegisterObserver(IObserver observer);
        void RemoveObserver(IObserver observer);
        void NotifyObservers();

    }
}
