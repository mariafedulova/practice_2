﻿using System;
using System.Collections.Generic;
using System.Text;

namespace practice_2
{
    abstract class Video: IObservable //паттерн наблюдатель
    {
        public string name { get; set; }

        List<IObserver> subscribers;

        public Video(string name)
        {
            subscribers = new List<IObserver>();
            this.name = name;
        }

        //декоратор
        public abstract string GetStatistics();

        //наблюдатель
        public void RegisterObserver(IObserver subscriber) 
        {
            subscribers.Add(subscriber);
        }
        public void RemoveObserver(IObserver subscriber) 
        {
            subscribers.Remove(subscriber);
        }
        public void NotifyObservers() 
        {
            foreach (IObserver subscriber in subscribers)
            {
                subscriber.Update();
            }
        }

    }

    class VideoClip: Video
    {
        public VideoClip(string name): base(name) { }

        //декоратор
        public override string GetStatistics()
        {
            return $"Статистика по видеоролику '{this.name}': ";
        }
    }

    class Stream: Video
    {
        public Stream(string name) : base(name) { }

        //декоратор
        public override string GetStatistics()
        {
            return $"Статистика по стриму '{this.name}': ";
        }
    }
}
