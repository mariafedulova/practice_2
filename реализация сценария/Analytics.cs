﻿using System;
using System.Collections.Generic;
using System.Text;

namespace practice_2
{

    //паттерн декоратор

    // Класс Analytics, который будет добавлять к объекту типа Video
    // информацию о количестве просмотров, оценок, комментариях
    abstract class Analytics: Video
    {
        public Random random = new Random();
        protected Video video;

        public Analytics(string name, Video video): base(name) 
        {
            this.video = video;
        }

        public override string GetStatistics()
        {
            return video.GetStatistics();
        }
    }

    class Views: Analytics
    {
        public Views(Video video): base(video.name, video) { }

        public override string GetStatistics()
        {
            return base.GetStatistics() +  $"\n просмотры: {random.Next(1, 1000)} \n";
        }
    }

    class Comments: Analytics
    {
        public Comments(Video video): base(video.name, video) { }
        public override string GetStatistics()
        {
            return base.GetStatistics() + $"\n комментарии: {random.Next(1, 50)}\n";
        }
    }

    class Rating : Analytics
    {
        public Rating(Video video): base(video.name, video) { }

        public override string GetStatistics()
        {
            return base.GetStatistics() + $"\n оценки: {random.Next(1, 100)}\n";
        }

    }

}
