﻿using System;
using System.Collections.Generic;
using System.Text;

namespace practice_2
{
    //паттерн фабричный метод
    //абстрактный класс Video и два класса-наследника: VideoClip и Stream
    //
    //Затем фабрикa, которая будет создавать объекты типа Video в зависимости от выбранного пользователем варианта

    //абстрактный класс создателя
    abstract class Creator
    {
        public abstract Video Create(string name);
    }

    //класс создателя для видео 
    class VideoCreator: Creator
    {
        public override Video Create(string name)
        {
            return new VideoClip(name);
        }
    }

    //класс создателя для стрима
    class StreamCreator : Creator
    {
        public override Video Create(string name)
        {
            return new Stream(name);
        }
    }
}
