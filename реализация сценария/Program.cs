﻿using System;

namespace practice_2
{
    class Program
    {
        static void Main(string[] args)
        {

            Video video;
            Console.WriteLine("Выберете тип видео: видеоролик(0) стрим(1)");
            if (Console.ReadLine() == "0")
            {
                VideoCreator creator = new VideoCreator();
                Console.WriteLine("Введите название видеоролика: ");

               video = creator.Create(Console.ReadLine()); 
            }
            else
            {
                StreamCreator creator = new StreamCreator();
                Console.WriteLine("Введите название стрима: ");

                video = creator.Create(Console.ReadLine());
            }

            Subscriber subscriber = new Subscriber(video);

            video.NotifyObservers();

            Console.WriteLine("Хотите посмотреть статистику? y/n");

            if (Console.ReadLine()== "y")
            {
                Views views = new Views(video);
                Comments comments = new Comments(video);
                Rating rating = new Rating(video);
                Console.WriteLine(views.GetStatistics() + comments.GetStatistics() + rating.GetStatistics());
            }

            subscriber.Unsubscribe();

        }
    }
}
