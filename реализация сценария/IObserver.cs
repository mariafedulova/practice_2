﻿using System;
using System.Collections.Generic;
using System.Text;

namespace practice_2
{
    //паттерн наблюдатель
    // Класс Subscriber, который будет подписываться на уведомления о новых видео
    // Класс Video будет являться наблюдаемым объектом

    interface IObserver
    {
        void Update();
    }

    class Subscriber: IObserver
    {
        IObservable video;

        public Subscriber(IObservable video)
        {
            this.video = video;
            this.video.RegisterObserver(this);
        }
        public void Update()
        {
            Console.WriteLine("Мне пришло уведомление и я побежал смотреть видео");
        }

        public void Unsubscribe()
        {
            video.RemoveObserver(this);
            video = null;
            Console.WriteLine("Мне больше не нравятся видео");
        }
    }
}
